﻿namespace KinectPiPi.Webserver
{
    /// <summary>
    /// Factory of <see cref="IHttpRequestHandler"/> objects.
    /// </summary>
    public interface IHttpRequestHandlerFactory
    {
        /// <summary>
        /// Creates a request handler object.
        /// </summary>
        /// <returns>
        /// A new <see cref="IHttpRequestHandler"/> instance.
        /// </returns>
        IHttpRequestHandler CreateHandler();
    }
}
