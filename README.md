![00.jpg](https://bitbucket.org/repo/geMrEy/images/2973281181-00.jpg)

### 功能概述 ###

* 轉換Kinect影像至網頁瀏覽器中，並支援同時兩位使用者去背的功能
* Web資料夾內提供基本的網頁使用者介面
* 更多功能可以藉由編輯C#核心程式碼來完成

### 系統需求 ###
|
:----------------| :-------------------------|
處理器 | 支援 DualCore 2.66GHz
顯示卡 | 支援 DirectX 9.0c
記憶體 | 至少 4.00GB
匯流排 | 支援 USB 2.0
作業系統 | Windows 8/8.1 (64-bit)
開發工具 | Visual Studio 2013 以上
開發環境 | Microsoft .NET Framework 4.0 以上
硬體設備 | Kinect for Windows V1 or Kinect for Xbox 360

### 專案簡介 ###

原先微軟提供的[WebserverBasics-WPF](http://msdn.microsoft.com/en-us/library/dn435689.aspx)範例中使用到了以下三個專案：

1. Microsoft.Kinect.Toolkit
2. Microsoft.Samples.Kinect.Webserver
3. WebserverBasics-WPF

>![01.jpg](https://bitbucket.org/repo/geMrEy/images/2292938072-01.jpg)

但在KinectPiPi中則將所有功能都集成在單一個專案內方便管理，注意使用前仍需安裝[Kinect SDK](http://www.microsoft.com/en-us/kinectforwindows/develop/downloads-docs.aspx)來調用函式庫與連接Kinect鏡頭

>![02.jpg](https://bitbucket.org/repo/geMrEy/images/778132993-02.jpg)